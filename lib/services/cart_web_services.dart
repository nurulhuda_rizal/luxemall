import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:luxemall/models/cart_model.dart';

class CartWebServices {
  String baseURL = "https://fakestoreapi.com";

  Future<Cart> addToCart(Cart cart) async {
    String apiURL = "$baseURL/carts";
    Uri uri = Uri.parse(apiURL);
    // print("port add ${jsonEncode(port.toMap())}");
    var apiResult = await http.post(uri,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
        body: jsonEncode(cart.toMap()));
    var jsonObject = json.decode(apiResult.body);
    return Cart.fromMap(jsonObject);
  }
}
