import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:luxemall/models/product_model.dart';

class ProductWebService {
  String baseURL = "https://fakestoreapi.com";

  Future<List<Product>> getGeneralProductList({int limit, String sort}) async {
    String apiURL = "$baseURL/products?limit=$limit&sort=$sort";
    Uri uri = Uri.parse(apiURL);
    var apiResult = await http.get(
      uri,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
    );
    var jsonObject = jsonDecode(apiResult.body);
    List<dynamic> productList = List.from(jsonObject);
    List<Product> products = [];
    for (int i = 0; i < productList.length; i++) {
      products.add(Product.fromMap(productList[i]));
    }
    return products;
  }

  Future<List<Product>> getCategorizedProductList(
      {int limit, String category, String sort}) async {
    String apiURL =
        "$baseURL/products/category/$category?limit=$limit&sort=$sort";
    Uri uri = Uri.parse(apiURL);
    var apiResult = await http.get(
      uri,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
    );
    var jsonObject = jsonDecode(apiResult.body);
    List<dynamic> productList = List.from(jsonObject);
    List<Product> products = [];
    for (int i = 0; i < productList.length; i++) {
      products.add(Product.fromMap(productList[i]));
    }
    return products;
  }

  Future<Product> getProduct(int id) async {
    String apiURL = "$baseURL/products/$id";
    Uri uri = Uri.parse(apiURL);
    var apiResult = await http.get(
      uri,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
    );
    var jsonObject = jsonDecode(apiResult.body);

    return Product.fromMap(jsonObject);
  }
}
