import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/bloc/cart/cart_bloc.dart';
import 'package:luxemall/bloc/product/product_bloc.dart';
import 'package:luxemall/views/screen/main_menu.dart';
import 'package:luxemall/views/screen/product_detail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LUXEMALL',
      theme: ThemeData(
        bottomSheetTheme:
            BottomSheetThemeData(backgroundColor: Colors.transparent),
        primarySwatch: Colors.grey,
      ),
      debugShowCheckedModeBanner: false,
      home: MultiBlocProvider(
        providers: [
          BlocProvider<ProductBloc>(
            create: (context) => ProductBloc(
              InitialProductState(),
            ),
          ),
          BlocProvider<CartBloc>(
            create: (context) => CartBloc(
              InitialCartState(),
            ),
          )
        ],
        child: MainMenu(),
      ),
      routes: <String, WidgetBuilder>{
        "/ProductDetail": (BuildContext context) {
          Map args = ModalRoute.of(context).settings.arguments;
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProductBloc>(
                create: (context) => ProductBloc(
                  InitialProductState(),
                ),
              ),
              BlocProvider<CartBloc>(
                create: (context) => CartBloc(
                  InitialCartState(),
                ),
              )
            ],
            child: ProductDetail(
              productId: args["productId"],
            ),
          );
        },
      },
    );
  }
}
