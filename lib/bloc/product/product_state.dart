part of 'product_bloc.dart';

@immutable
abstract class ProductState {
  final Product product;
  final String message;
  final List<Product> products;
  final int page;
  final bool hasReachedMax;

  ProductState({
    this.product,
    this.message,
    this.products,
    this.page,
    this.hasReachedMax = false,
  });
}

class InitialProductState extends ProductState {}

class Loading extends ProductState {}

class FormError extends ProductState {
  FormError({@required String errorMessage}) : super(message: errorMessage);
}

class FormLoaded extends ProductState {
  FormLoaded({@required Product product}) : super(product: product);
}

class Success extends ProductState {
  Success({@required successMessage, Product product})
      : super(message: successMessage, product: product);
}

class ListLoaded extends ProductState {
  ListLoaded({
    @required List<Product> products,
    int page,
    bool hasReachedMax,
    Product product,
  }) : super(
          products: products,
          page: page,
          hasReachedMax: hasReachedMax,
          product: product,
        );
}

class ListError extends ProductState {
  ListError({@required String errorMessage}) : super(message: errorMessage);
}

class LoadingList extends ProductState {}
