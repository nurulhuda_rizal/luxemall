part of 'product_bloc.dart';

abstract class ProductEvent {
  final int id;
  final Product product;
  final int limit;
  final int page;
  final String category;
  final String sort;

  ProductEvent({
    this.id,
    this.product,
    this.limit,
    this.page,
    this.category,
    this.sort,
  });
}

class BackEvent extends ProductEvent {}

class GetProduct extends ProductEvent {
  GetProduct({@required int id}) : super(id: id);
}

class GetProducts extends ProductEvent {
  GetProducts({String category, int limit, int page, String sort})
      : super(category: category, limit: limit, page: page, sort: sort);
}

class ReloadProducts extends ProductEvent {
  ReloadProducts({String category, int limit, int page, String sort})
      : super(category: category, limit: limit, page: page, sort: sort);
}

class LoadProducts extends ProductEvent {}
