import 'dart:io';

import 'package:bloc/bloc.dart';

import 'package:flutter/material.dart';
import 'package:luxemall/models/product_model.dart';
import 'package:luxemall/services/product_web_services.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc(ProductState initialState) : super(InitialProductState());
  final ProductWebService _webService = ProductWebService();
  final int perPage = 50;

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is GetProduct) {
      try {
        yield Loading();
        yield FormLoaded(
            product: event.id == null
                ? Product()
                : await _webService.getProduct(event.id));
      } on SocketException catch (_) {
        yield FormError(errorMessage: "No internet connection");
      }
    } else if (event is BackEvent) {
      yield InitialProductState();
    } else if (event is GetProducts) {
      try {
        List<Product> productsFromAPI;
        if (!state.hasReachedMax) {
          int limit = event.limit * event.page;
          if (event.page > 1) {
            if (event.category == "all") {
              productsFromAPI = await _webService.getGeneralProductList(
                  limit: limit, sort: event.sort);
            } else {
              productsFromAPI = await _webService.getCategorizedProductList(
                  limit: limit, category: event.category, sort: event.sort);
            }
            int page = event.page + 1;
            // print(productsFromAPI);
            if (productsFromAPI.length < limit) {
              yield ListLoaded(
                products: productsFromAPI,
                page: page,
                hasReachedMax: true,
                product: Product(),
              );
            } else {
              yield ListLoaded(
                products: productsFromAPI,
                page: page,
                hasReachedMax: false,
                product: Product(),
              );
            }
          } else {
            yield Loading();
            if (event.category == "all") {
              productsFromAPI = await _webService.getGeneralProductList(
                  limit: limit, sort: event.sort);
            } else {
              productsFromAPI = await _webService.getCategorizedProductList(
                  limit: limit, category: event.category, sort: event.sort);
            }
            int page = event.page + 1;
            // print(productsFromAPI);
            if (productsFromAPI.length < limit) {
              yield ListLoaded(
                products: productsFromAPI,
                page: page,
                hasReachedMax: true,
                product: Product(),
              );
            } else {
              yield ListLoaded(
                products: productsFromAPI,
                page: page,
                hasReachedMax: false,
                product: Product(),
              );
            }
          }
        }
      } catch (e) {
        yield FormError(errorMessage: "No internet connection");
      }
    } else if (event is ReloadProducts) {
      try {
        List<Product> productsFromAPI;

        int limit = event.limit * event.page;
        if (event.page > 1) {
          if (event.category == "all") {
            productsFromAPI = await _webService.getGeneralProductList(
                limit: limit, sort: event.sort);
          } else {
            productsFromAPI = await _webService.getCategorizedProductList(
                limit: limit, category: event.category, sort: event.sort);
          }
          int page = event.page + 1;
          // print(productsFromAPI);
          if (productsFromAPI.length < limit) {
            yield ListLoaded(
              products: productsFromAPI,
              page: page,
              hasReachedMax: true,
              product: Product(),
            );
          } else {
            yield ListLoaded(
              products: productsFromAPI,
              page: page,
              hasReachedMax: false,
              product: Product(),
            );
          }
        } else {
          yield Loading();
          if (event.category == "all") {
            productsFromAPI = await _webService.getGeneralProductList(
                limit: limit, sort: event.sort);
          } else {
            productsFromAPI = await _webService.getCategorizedProductList(
                limit: limit, category: event.category, sort: event.sort);
          }
          int page = event.page + 1;
          // print(productsFromAPI);
          if (productsFromAPI.length < limit) {
            yield ListLoaded(
              products: productsFromAPI,
              page: page,
              hasReachedMax: true,
              product: Product(),
            );
          } else {
            yield ListLoaded(
              products: productsFromAPI,
              page: page,
              hasReachedMax: false,
              product: Product(),
            );
          }
        }
      } catch (e) {
        yield FormError(errorMessage: "No internet connection");
      }
    } else if (event is LoadProducts) {
      yield Loading();
    }
  }
}
