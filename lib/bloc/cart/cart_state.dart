part of 'cart_bloc.dart';

@immutable
abstract class CartState {
  final Cart cart;
  final String message;
  final List<Cart> carts;
  final int page;
  final bool hasReachedMax;

  CartState({
    this.cart,
    this.message,
    this.carts,
    this.page,
    this.hasReachedMax = false,
  });
}

class InitialCartState extends CartState {}

class Loading extends CartState {}

class FormError extends CartState {
  FormError({@required String errorMessage}) : super(message: errorMessage);
}

class CartFormLoaded extends CartState {
  CartFormLoaded({@required Cart cart}) : super(cart: cart);
}

class CartSuccess extends CartState {
  CartSuccess({@required successMessage, Cart cart})
      : super(message: successMessage, cart: cart);
}

class CartListLoaded extends CartState {
  CartListLoaded({
    @required List<Cart> carts,
    int page,
    bool hasReachedMax,
    Cart cart,
  }) : super(
          carts: carts,
          page: page,
          hasReachedMax: hasReachedMax,
          cart: cart,
        );
}

class ListError extends CartState {
  ListError({@required String errorMessage}) : super(message: errorMessage);
}

class LoadingList extends CartState {}
