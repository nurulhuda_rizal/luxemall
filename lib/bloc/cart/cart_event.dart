part of 'cart_bloc.dart';

abstract class CartEvent {
  final int id;
  final int userId;
  final Cart cart;
  final int limit;
  final int page;
  final String sort;

  CartEvent({
    this.id,
    this.userId,
    this.cart,
    this.limit,
    this.page,
    this.sort,
  });
}

class BackEvent extends CartEvent {}

class AddToCart extends CartEvent {
  AddToCart({Cart cart}) : super(cart: cart);
}

class GetUserCart extends CartEvent {
  GetUserCart({@required int userId}) : super(userId: userId);
}

class GetCarts extends CartEvent {
  GetCarts({int limit, int page, String sort})
      : super(limit: limit, page: page, sort: sort);
}

class ReloadUserCarts extends CartEvent {
  ReloadUserCarts({int limit, int page, String sort})
      : super(limit: limit, page: page, sort: sort);
}

class LoadCarts extends CartEvent {}
