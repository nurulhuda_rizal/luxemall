import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:luxemall/models/cart_model.dart';
import 'package:luxemall/services/cart_web_services.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc(CartState initialState) : super(InitialCartState());
  final CartWebServices _webService = CartWebServices();
  final int perPage = 50;

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is AddToCart) {
      try {
        yield Loading();
        Cart cart = await _webService.addToCart(event.cart);
        yield CartSuccess(successMessage: "added to cart", cart: cart);
      } on SocketException catch (_) {
        yield FormError(errorMessage: "No internet connection");
      }
    } else if (event is LoadCarts) {
      yield Loading();
    }
  }
}
