import 'package:flutter/material.dart';

class CustomElevation extends StatelessWidget {
  final Widget child;
  final Color color;

  CustomElevation({@required this.child, this.color}) : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: this.color != null
                ? this.color.withOpacity(0.4)
                : Colors.grey.withOpacity(0.3),
            blurRadius: 10,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: this.child,
    );
  }
}
