import 'package:flutter/material.dart';
import 'package:luxemall/bloc/cart/cart_bloc.dart';
import 'package:luxemall/bloc/product/product_bloc.dart';
import 'package:luxemall/models/product_model.dart';
import 'package:luxemall/utils/my_colors.dart';

import 'custom_elevation.dart';

class ProductCard extends StatefulWidget {
  final Product product;
  final ProductBloc productBloc;
  final CartBloc cartBloc;
  ProductCard({this.product, this.productBloc, this.cartBloc});
  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Align(
        child: CustomElevation(
          child: Container(
            width: 166,
            height: 246,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: MyColors().white),
            child: InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 166,
                    width: 166,
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(15)),
                        color: Colors.grey),
                    child: ClipRRect(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(15)),
                      child: Image.network(
                        '${widget.product.image}',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${widget.product.title}",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: MyColors().grey),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          "${widget.product.price} USD",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: MyColors().gold),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          "${widget.product.category}",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              color: MyColors().grey),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  "/ProductDetail",
                  arguments: {
                    "productId": widget.product.id,
                    "productBloc": widget.productBloc,
                    "cartBloc": widget.cartBloc,
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
