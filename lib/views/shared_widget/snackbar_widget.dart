import 'package:flutter/material.dart';
import 'package:luxemall/utils/my_colors.dart';

SnackBar snackBar({String message, Duration duration}) {
  return SnackBar(
    content: Text(
      message,
      style: TextStyle(
        fontSize: 13,
        fontWeight: FontWeight.w500,
        color: Colors.white,
      ),
    ),
    duration: duration,
    backgroundColor: MyColors().grey,
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
}
