import 'package:luxemall/utils/my_colors.dart';
import 'package:flutter/material.dart';

Widget loading() {
  return Center(
      child: CircularProgressIndicator(
    valueColor: AlwaysStoppedAnimation<Color>(MyColors().gold),
  ));
}
