import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/bloc/cart/cart_bloc.dart';
import 'package:luxemall/bloc/product/product_bloc.dart';
import 'package:luxemall/models/product_model.dart';
import 'package:luxemall/utils/my_colors.dart';
import 'package:luxemall/views/shared_widget/loading_widget.dart';
import 'package:luxemall/views/shared_widget/product_card.dart';

class ProductCatalog extends StatefulWidget {
  final ProductBloc productBloc;
  final CartBloc cartBloc;
  ProductCatalog({this.productBloc, this.cartBloc});
  @override
  _ProductCatalogState createState() => _ProductCatalogState();
}

class _ProductCatalogState extends State<ProductCatalog> {
  int limit = 4;
  int page;
  List<Product> products;
  int productIndex = 4;
  final _scrollController = ScrollController();

  final List<Map> _sort = [
    {"value": "asc", "text": "Ascending"},
    {"value": "desc", "text": "Descending"},
  ];
  String sort = "asc";
  final List<Map> _category = [
    {"value": "all", "text": "All"},
    {"value": "electronics", "text": "Electronics"},
    {"value": "jewelery", "text": "Jewelery"},
    {"value": "men's clothing", "text": "Men's clothing"},
    {"value": "women's clothing", "text": "Women's clothing"},
  ];
  String category = "all";
  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (currentScroll == maxScroll) {
      // print("loadnew");
      widget.productBloc.add(GetProducts(
          category: category, limit: limit, page: page, sort: sort));
    }
  }

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    widget.productBloc.add(
        GetProducts(category: category, limit: limit, page: 1, sort: sort));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors().gold,
        title: Text(
          "Catalog",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: MyColors().white,
          ),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: MyColors().white,
        child: Stack(
          children: [
            Center(
              child: BlocBuilder<ProductBloc, ProductState>(
                builder: (context, state) {
                  if (state is ListLoaded) {
                    page = state.page;
                    products = state.products;
                    if (state.products == null || state.products.length == 0) {
                      return Container(
                        child: Text("No data"),
                      );
                    } else {
                      return ListView(
                        controller: _scrollController,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 48,
                          ),
                          GridView.builder(
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 3 / 4.3,
                                    crossAxisSpacing: 0,
                                    mainAxisSpacing: 0),
                            itemCount: state.hasReachedMax
                                ? state.products.length
                                : state.products.length + 2,
                            itemBuilder: (context, index) {
                              productIndex = index;
                              return index >= state.products.length
                                  ? Container(
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: SizedBox(
                                          width: 33,
                                          height: 33,
                                          child: loading(),
                                        ),
                                      ),
                                    )
                                  : Center(
                                      child: ProductCard(
                                        product: products[index],
                                        productBloc: widget.productBloc,
                                        cartBloc: widget.cartBloc,
                                      ),
                                    );
                            },
                          ),
                        ],
                      );
                    }
                  }
                  return loading();
                },
              ),
            ),
            Container(
              width: double.infinity,
              height: 66,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 210,
                        child: DropdownButtonFormField(
                          items: _category
                              .map(
                                (e) => DropdownMenuItem(
                                  value: e["value"],
                                  child: Text(
                                    "Category: " + e["text"],
                                    style: TextStyle(
                                      color: MyColors().grey,
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                          style:
                              TextStyle(fontSize: 13, color: MyColors().grey),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(12),
                            filled: true,
                            fillColor: Colors.white,
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            isDense: true,
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(
                              color: MyColors().grey,
                              fontSize: 13,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          onChanged: (value) {
                            if (category != value) {
                              print("get");
                              setState(() {
                                category = value;
                              });
                              widget.productBloc.add(ReloadProducts(
                                  category: category,
                                  limit: limit,
                                  page: 1,
                                  sort: sort));
                            }
                          },
                          value: category,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 148,
                        child: DropdownButtonFormField(
                          items: _sort
                              .map(
                                (e) => DropdownMenuItem(
                                  value: e["value"],
                                  child: Text(
                                    "Sort: " + e["text"],
                                    style: TextStyle(
                                      color: MyColors().grey,
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                          style:
                              TextStyle(fontSize: 13, color: MyColors().grey),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(12),
                            filled: true,
                            fillColor: Colors.white,
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            isDense: true,
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(
                              color: MyColors().grey,
                              fontSize: 13,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          onChanged: (value) {
                            if (value != sort) {
                              setState(() {
                                sort = value;
                              });
                              widget.productBloc.add(ReloadProducts(
                                  category: category,
                                  limit: limit,
                                  page: 1,
                                  sort: sort));
                            }
                          },
                          value: sort,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
