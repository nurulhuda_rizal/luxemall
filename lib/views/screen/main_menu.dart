import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/bloc/cart/cart_bloc.dart';
import 'package:luxemall/bloc/product/product_bloc.dart';
import 'package:luxemall/utils/my_colors.dart';
import 'package:luxemall/views/screen/cart_screen.dart';
import 'package:luxemall/views/screen/product_catalog.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu>
    with SingleTickerProviderStateMixin {
  ProductBloc productBloc;
  CartBloc cartBloc;
  PageController _pageController = PageController();
  List<Widget> _screens() => [
        ProductCatalog(
          productBloc: productBloc,
          cartBloc: cartBloc,
        ),
        CartScreen(),
      ];

  int selectedIndex = 0;
  void _onPageChanged(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void _onTabBarItemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    productBloc = BlocProvider.of<ProductBloc>(context);
    cartBloc = BlocProvider.of<CartBloc>(context);
    final List<Widget> screens = _screens();
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomAppBar(
        color: MyColors().gold,
        elevation: 15.0,
        child: SizedBox(
          height: 55,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                height: 45,
                child: TextButton(
                  style: TextButton.styleFrom(
                      primary: MyColors().white,
                      visualDensity: VisualDensity.compact),
                  child: Column(children: [
                    Icon(Icons.auto_stories_rounded),
                    Text("Catalog"),
                  ]),
                  onPressed: () {
                    _onTabBarItemTapped(0);
                  },
                ),
              ),
              SizedBox(
                height: 45,
                child: TextButton(
                  style: TextButton.styleFrom(
                      primary: MyColors().white,
                      visualDensity: VisualDensity.compact),
                  child: Column(children: [
                    Icon(Icons.shopping_cart_rounded),
                    Text("Cart"),
                  ]),
                  onPressed: () {
                    _onTabBarItemTapped(1);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
