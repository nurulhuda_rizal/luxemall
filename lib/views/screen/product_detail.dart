import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/bloc/cart/cart_bloc.dart';
import 'package:luxemall/bloc/product/product_bloc.dart';
import 'package:luxemall/models/cart_model.dart';
import 'package:luxemall/models/product_model.dart';
import 'package:luxemall/utils/my_colors.dart';
import 'package:luxemall/views/shared_widget/custom_elevation.dart';
import 'package:luxemall/views/shared_widget/loading_widget.dart';
import 'package:luxemall/views/shared_widget/snackbar_widget.dart';

class ProductDetail extends StatefulWidget {
  final int productId;
  ProductDetail({this.productId});
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  Product product;
  CartBloc cartBloc;

  @override
  void initState() {
    context.read<ProductBloc>().add(GetProduct(id: widget.productId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    cartBloc = BlocProvider.of<CartBloc>(context);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: MyColors().white, //change your color here
        ),
        titleSpacing: 0,
        backgroundColor: MyColors().gold,
        title: BlocBuilder<ProductBloc, ProductState>(
          builder: (context, state) => Text(
            state.product?.title != null
                ? "${product.title}"
                : "Product Detail",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: MyColors().white,
            ),
          ),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: MyColors().white,
        child: BlocListener<CartBloc, CartState>(
          listenWhen: (previousState, state) {
            return state is CartSuccess;
          },
          listener: (context, state) {
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(
              snackBar(
                message:
                    "${state.cart.products[0].quantity} ${product.title} has been added to cart",
                duration: Duration(
                  milliseconds: 3000,
                ),
              ),
            );
          },
          child: BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
              if (state is FormLoaded) {
                product = state.product;
                return Stack(
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 300,
                            color: Colors.white,
                            child: Image.network(
                              '${product.image}',
                              width: MediaQuery.of(context).size.width,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 18,
                                ),
                                Text(
                                  "${product.title}",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: MyColors().grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "${product.price} USD",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: MyColors().gold,
                                  ),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "${product.category}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: MyColors().grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 18,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width - 5,
                                  height: 2,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(1)),
                                    color: MyColors().gold,
                                  ),
                                ),
                                SizedBox(
                                  height: 18,
                                ),
                                Text(
                                  "Description:",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: MyColors().grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "${product.description}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: MyColors().grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 18,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width - 5,
                                  height: 2,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(1)),
                                      color: MyColors().gold),
                                ),
                                SizedBox(
                                  height: 18,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              }
              return loading();
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text(
          "Add to cart",
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: MyColors().white),
        ),
        icon: Icon(Icons.add_shopping_cart_rounded, color: MyColors().white),
        backgroundColor: MyColors().gold,
        onPressed: () {
          _showAddToCartDialog(context, product);
        },
      ),
    );
  }

  void _showAddToCartDialog(BuildContext context, Product product) {
    String stringQty = "1";
    int qty = 1;
    Cart cart = Cart();
    TextEditingController textControler =
        TextEditingController(text: stringQty);
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
      backgroundColor: MyColors().white,
      isScrollControlled: true,
      context: context,
      builder: (BuildContext bc) {
        return StatefulBuilder(
          builder: (BuildContext context, setState) {
            return Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      child: Text(
                        "Add to cart",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: MyColors().grey,
                        ),
                      ),
                    ),
                    Container(
                      height: 16,
                    ),
                    CustomElevation(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: MyColors().white),
                        child: Row(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(15)),
                                  color: Colors.grey),
                              child: ClipRRect(
                                borderRadius: BorderRadius.horizontal(
                                    left: Radius.circular(15)),
                                child: Image.network(
                                  '${product.image}',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              width: 8,
                            ),
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${product.title}",
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: MyColors().grey,
                                    ),
                                  ),
                                  Container(
                                    height: 4,
                                  ),
                                  Text(
                                    "${product.price} USD",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: MyColors().gold,
                                    ),
                                  ),
                                  Container(
                                    height: 4,
                                  ),
                                  Text(
                                    "${product.category}",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: MyColors().grey,
                                    ),
                                  ),
                                  Container(
                                    height: 6,
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        height: 22,
                                        width: 22,
                                        child: TextButton(
                                            style: TextButton.styleFrom(
                                              primary: MyColors().white,
                                              backgroundColor: MyColors().gold,
                                              visualDensity:
                                                  VisualDensity.compact,
                                              padding: EdgeInsets.zero,
                                            ),
                                            onPressed: () {
                                              if (qty > 1) {
                                                setState(() {
                                                  qty--;
                                                  textControler.text =
                                                      qty.toString();
                                                });
                                              }
                                            },
                                            child: Text("-")),
                                      ),
                                      Container(
                                        width: 6,
                                      ),
                                      SizedBox(
                                        height: 22,
                                        width: 30,
                                        child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: textControler,
                                            cursorColor: MyColors().grey,
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500,
                                              color: MyColors().grey,
                                            ),
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      5, 5, 5, 5),
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.never,
                                              isDense: true,
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                            ),
                                            onChanged: (value) {
                                              setState(() {
                                                qty = int.parse(value);
                                              });
                                            }),
                                      ),
                                      Container(
                                        width: 6,
                                      ),
                                      SizedBox(
                                        height: 22,
                                        width: 22,
                                        child: TextButton(
                                          style: TextButton.styleFrom(
                                            primary: MyColors().white,
                                            backgroundColor: MyColors().gold,
                                            visualDensity:
                                                VisualDensity.compact,
                                            padding: EdgeInsets.zero,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              qty++;
                                              textControler.text =
                                                  qty.toString();
                                            });
                                          },
                                          child: Icon(
                                            Icons.add,
                                            size: 16,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 8,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 16,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: CustomElevation(
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: MyColors().white,
                            backgroundColor: MyColors().gold,
                          ),
                          child: Text("Add"),
                          onPressed: () {
                            cart.userId = 14;
                            cart.date = DateTime.now().toIso8601String();
                            ProductsInCart productsInCart = ProductsInCart(
                                productId: product.id, quantity: qty);
                            List<ProductsInCart> products = <ProductsInCart>[];
                            products.add(productsInCart);
                            cart.products = products;
                            print(jsonEncode(cart.toMap()));
                            cartBloc.add(AddToCart(cart: cart));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
