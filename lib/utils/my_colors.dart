import 'package:flutter/material.dart';

class MyColors {
  Color gold = Color.fromRGBO(194, 145, 46, 1.0);
  Color white = Color.fromRGBO(255, 255, 255, 1.0);
  Color grey = Color.fromRGBO(88, 88, 88, 1.0);
  Color greyTrans = Color.fromRGBO(88, 88, 88, 0.5);
}
