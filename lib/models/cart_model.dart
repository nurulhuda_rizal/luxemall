class Cart {
  int id;
  int userId;
  String date;
  List<ProductsInCart> products;

  Cart({this.id, this.userId, this.date, this.products});

  Map<String, dynamic> toMap() {
    List<Map> products = this.products != null
        ? this.products.map((e) => e.toMap()).toList()
        : null;
    final cart = Map<String, dynamic>();
    if (id != null) cart["id"] = id;
    if (userId != null) cart["userId"] = userId;
    if (date != null) cart["date"] = date;
    if (products != null) cart["products"] = products;
    return cart;
  }

  factory Cart.fromMap(Map<String, dynamic> cart) {
    List productsList = cart["products"];
    List<ProductsInCart> products = productsList != null
        ? productsList.map((e) => ProductsInCart.fromMap(e)).toList()
        : null;
    return Cart(
      id: cart["id"],
      userId: cart["userId"],
      date: cart["date"],
      products: products != null ? products : [],
    );
  }
}

class ProductsInCart {
  int productId;
  int quantity;

  ProductsInCart({this.productId, this.quantity});

  Map<String, dynamic> toMap() {
    final product = Map<String, dynamic>();
    if (productId != null) product["productId"] = productId;
    if (quantity != null) product["quantity"] = quantity;

    return product;
  }

  factory ProductsInCart.fromMap(Map<String, dynamic> product) {
    return ProductsInCart(
      productId: product["productId"],
      quantity: product["quantity"],
    );
  }
}
