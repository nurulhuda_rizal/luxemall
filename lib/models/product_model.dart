class Product {
  int id;
  String title;
  double price;
  String category;
  String description;
  String image;

  Product({
    this.id,
    this.title,
    this.price,
    this.category,
    this.description,
    this.image,
  });

  Map<String, dynamic> toMap() {
    final product = Map<String, dynamic>();
    product["id"] = id;
    product["title"] = title;
    product["price"] = price;
    product["category"] = category;
    product["description"] = description;
    product["image"] = image;
    return product;
  }

  factory Product.fromMap(Map<String, dynamic> product) {
    return Product(
      id: product["id"],
      title: product["title"],
      price: double.parse(product["price"].toString()),
      category: product["category"],
      description: product["description"],
      image: product["image"],
    );
  }
}
